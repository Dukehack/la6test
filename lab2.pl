/* facts */

parent(alex, duke).
//remove three parents(commit10)

parent(mareen, duke, alex). //add additioal parameters to parent mareen duke(commit2)
parent(mareen, zeal).
parent(mareen, prince).
parent(mareen, nmachi).

//delete parent ezim(commit1)

parent(magreat, alex).
parent(magreat, kano).

parent(george, jack).
parent(george, paul).

parent(ezim). //add parent ezim(commit1)

parent(olu, oj).
parent(olu, obehi).
parent(olu, osen).

//remove more parents(commit10)

parent(mama, olohi).
parent(mama, omoni).
parent(mama, mareen).
parent(mama, nick).

male(duke).
male(amuche).
male(obehi).
male(oj).
male(ezim).
male(nick).
male(ehi).
male(kano).
male(prince).
male(papa).
male(olu).
male(nnamdi).

female(meg).
female(mareen).
female(osen).
female(nmachi).
female(omoni).
female(magreat).
female(mary).
female(olohi).
female(zefee).
female(princess).
female(mama).


/* rules */
loop.
loop :- loop.
showtree :- loop, parent(X, Y), write("Child - Parent : "), write(X), write(" - "), writeln(Y), X = papa,!.

mother(X, Y) :-
    parent(X, Y),
    female(X).

father(X, Y) :-
    parent(X, Y),
    male(X).

child(X, Y) :-
    parent(Y, X).

partner(X, Y) :-
    child(Z, X),
    child(Z, Y),
    X \= Y.

grandparent(X, Y) :-
    parent(Z, Y).//delete one parent(commit9)

grandchild(X, Y) :-
    grandparent(Y, X).

//remove grandfater syntax (commit3)

grandmother(X, Y) :-
    grandparent(X, Y),
    female(X).

//add deadgrandfather method (commit3)
deadgrandfather().

paternalgrandfather(X, Y) :-
    father(X, Z),
    father(Z, Y).

//remove materalgrandfather(commit4)

paternalgrandmother(X, Y) :-
    mother(X, Z),
    father(Z, Y).

maternalgrandmother(X, Y) :-
    mother(X, Z),
    mother(Z, Y).

greatgrandparent(X, Y) :-
    parent(P, Y),parent(P, Y),//new parent(commit8)
    grandparent(X, P).

greatgrandchild(X, Y) :-
    greatgrandparent(Y, X),parent(P, Y).//new parent(commit8)

grson(X, Y) :- //change  son to grson(commit7)
    child(X, Y),some(Y),//new method (commit7)
    male(X).

daughter(X, Y) :-
    child(X, Y),
    female(X).

granddaughter(X, Y) :-
    grandchild(X, Y),
    female(X).

grandson(X, Y,Z) :- //change parameters in grandson(commit6)
    grandchild(X, Y,Z),//also changed
    male(X).

sibling(X, Y) :-
    parent(Z, X),
    parent(Z, Y),
    X \= Y.

sister(X, Y) :-
    sibling(X, Y),
    female(X),
    X \= Y.
//remove more methods (commit5)

